# Nutanix Ansible Foundation

This is an playbook to configure and invoke Nutanix Foundation via Ansible to create a repeatable method for provisioning multiple new Nutanix clusters.


# Nutanix Foundation

Nutanix Foundation is a tool that allow administrators to completely bootstrap, deploy and configure a bare-metal Nutanix cluster from start-to-end with minimal interaction in matter of minutes. Nutanix Foundation will automatically configure IPMI, deploy and configure hypervisors, deploy and configure Nutanix Controller VMs and finally create a cluster with selected nodes.


## Using this repository

Simply download (clone) the repository and start modifying files according to your needs.

```
git clone https://gitlab.com/nutanix-se/nutanix-foundation-ansible ntnx-ansible-foundation/
```

Ideally, you'll want to use [Git](https://git-scm.com/) to manage your Ansible configuration files. For that purpose simply [fork](https://help.github.com/articles/fork-a-repo/) this repository into your own Git repository before cloning and customizing it. Git will allow you to version and roll-back changes with ease.

Specifically, you'll want to customize the following files:
- Update the string FOUNDATION_IP_ADDRESS in `inventory` to the ip address of your foundation virtual machine. Instructions on how to deploy a foundation can be found [here](https://portal.nutanix.com/#/page/docs/details?targetId=Field-Installation-Guide-v4-5:Field-Installation-Guide-v4-5).
- Copy the template cluster definition file based on `vars/_cluster_template.yml`. When copying this file ensure the filename matches your target cluster name.
- Modify the variables in the new cluster definition file, changing all options related to the planned configuration of the cluster.
  - Mandatory variables are uncommented in the template
  - Variable definitions are supplied adjacent to all variables in the template
- If using a hypervisor other than AHV copy the hypervisor iso into the `files/` directory. Ensure that the hypervisor version, checksum, checksum type and filename are present and correct in `vars/hypervisors.yml`.
  - The playbook will not attempt to download the hypervisor iso as distribution of these images are typically controlled by the hypervisor OEM.

## Executing Foundation

Install `ansible` on your laptop using the instructions found here https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html

To download all necessary components, check file integrity, upload files to the foundation vm, setup foundation and execute node imaging.
```
# Run all tasks in playbook
ansible-playbook site.yml -i inventory -c paramiko -e cluster_name="cluster_name"
```

### Working without internet access from foundation VM network
On a network with internet access first download all necessary components without attempting to communicate with the foundation vm.
```
# Run tasks to download all necessary components to configure foundation vm
ansible-playbook site.yml -i inventory -c paramiko -e cluster_name="cluster_name" -t download
```

Once all files reside in the local path of this playbook you can connect to the network where the foundation vm resides and execute the following command to upload files to the foundation vm, setup foundation and execute node imaging.
```
# Use previously downloaded components to configure & invoke foundation
ansible-playbook site.yml -i inventory -c paramiko -e cluster_name="cluster_name" -t offline
```

## Copyright and license

Copyright 2020 Ross Davies, released under the [MIT license](LICENSE)
